### 02.08.2017
### written by MegaPihar
* Here is docker container with open-ethereum-pool
### Commands for ubuntu16 on amd64 (run from root user):
### 1. Install docker and git:
```
apt-get install vim git apt-transport-https ca-certificates curl software-properties-common python-software-properties

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update

apt-get install docker-ce
```
### 2. Clone content of this repo
```
git clone https://bitbucket.org/Voltmod/gominingpool.git

cd gominingpool
```
### 3. Adjust Dockerfile with your domain or external ip
##### 3.1. Option #1 (IP)
* get external ip of your VM:
```
curl icanhazip.com
```
* replace "anus.com.ua" in Dockerfile with your external IP (here is example for ip 1.1.1.1)
```
sed -i 's/anus.com.ua/1.1.1.1/g' Dockerfile
```
##### 3.2. Option #2 (Domain)
* replace "anus.com.ua" in Dockerfile with your own domain (here is example for google.com domain:)
```
sed -i 's/anus.com.ua/google.com/g' Dockerfile
```
* !!! IMPORTANT: make sure that your own domain's IP in DNS match external IP of your VM
* you can check it with comparing of 2 commands:
```
ping -c1 your-own-domain.com

curl icanhazip.com
```
* external IP of your VM and IP of your domain in DNS - MUST be the same.
### (Optional) 4. Register ethereum account. It is 1 time operation. Skip it if you already have one.
* We installing ethereum locally (not within docker) in order to register account and then use it within Docker.
```
apt-get update

apt-get install python-software-properties software-properties-common -y

add-apt-repository -y ppa:ethereum/ethereum

apt-get update

apt-get install ethereum

geth account new
```
* enter your super secure password 2 times (dont forget it!!!) and remember your hash-id
* it will looks like b4a97ab047c027a9f62f4c7114d9f3c26f0b7a79
### (Optional) 5. Modify supervisord.conf with your hash-id and correct command of ethereum tool launching.
* by default supervisord.conf is configure to be launched like (line #10 in supervisord.conf file):
```
/usr/bin/geth --rpc --etherbase 'bd9ce756963e0e791cfaeb881862ded4a9c731f7' --mine
```
* but there are a lot of different options of launching those command
* please read carefully ethereum 
[https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options](https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options) 
documentation and adjust supervisord.conf with correcnt geth command, that you need.
```
vim supervisord.conf
```
### 6. Build docker container
```
docker build -t open-ethereum-pool ./
```
* it is time (and resource) consuming operation, please wait unless docker container will be built.
* it can take even 30-60 minutes (depends of CPU/RAM on your VM)

### 7. Start docker container and expose ports 80 6379 8080 8008 8888 30303 to the public:
```
docker run -dit -p 80:80 -p 6379:6379 -p 8080:8080 -p 8008:8008 -p 8888:8888 -p 30303:30303 --restart unless-stopped open-ethereum-pool
```
### 8. Check that docker container is running
```
docker ps
```
### 9. Check that you can access app via browser:
* go to
* http://<external-ip-of-your-VM> or http://<your-own-domain-name.com>
