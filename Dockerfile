FROM ubuntu:16.04
ENV DEBIAN_FRONTEND noninteractive 
#ENV ext_ip anus.com.ua
## installing all needed stuff
RUN apt-get update -y && \
          apt-get upgrade -y && \
          apt-get install -y --no-install-recommends apt-utils && \
          apt-get install -y python-software-properties software-properties-common && \
          add-apt-repository -y ppa:bitcoin/bitcoin && \
          add-apt-repository -y ppa:ethereum/ethereum && \
          apt-get update -y && \
          apt-get install -y supervisor libdb4.8-dev libdb4.8++-dev ethereum nginx git build-essential libtool autotools-dev autoconf pkg-config libssl-dev libboost-all-dev git npm nodejs nodejs-legacy libminiupnpc-dev redis-server golang && \
          apt-get clean && \
          rm -rf /var/lib/apt/lists/*

## git clone
RUN git config --global http.https://gopkg.in.followRedirects true && \
    cd /opt/ && \
    git clone https://github.com/sammy007/open-ethereum-pool.git

## compiling
RUN cd /opt/open-ethereum-pool && \
    mv config.example.json config.json && \
    sed -i 's/example.net/anus.com.ua/g' /opt/open-ethereum-pool/www/config/environment.js && \
    make && \
    cd /opt/open-ethereum-pool/www && \
    npm install -g ember-cli@2.9.1 && \
    npm install -g bower && \
    npm install && \
    bower install --allow-root && \
    ./build.sh

RUN mkdir -p /var/log/supervisor
COPY default /etc/nginx/sites-enabled/
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
EXPOSE 80 6379 8080 8008 8888 30303
CMD /usr/bin/supervisord -n
